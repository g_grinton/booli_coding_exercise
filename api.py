import asyncio
import numbers
import os
import statistics
from typing import List

import motor.motor_asyncio
import requests
from fastapi import Body, FastAPI, HTTPException
from fastapi.openapi.utils import get_openapi
from pydantic import BaseModel

from tasks import booli_api_auth, fetch

db = motor.motor_asyncio.AsyncIOMotorClient(
    f"mongodb://{os.getenv('MONGO_HOST')}")[os.getenv("MONGO_DB")]

app = FastAPI()


def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="Booli VärderingsAPI",
        version="0.0.1a",
        description="<img src='https://bcdn.se/images/resources/booli_logo.png' /><br/>An API to appraise a real estate item, based on the Booli API.",
        routes=app.routes,
    )
    openapi_schema["info"]["x-logo"] = {
        "url": "https://bcdn.se/images/resources/booli_logo.png"
    }
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi


class AppraiseField(BaseModel):
    """This model is used to describe a custom field used when appraising.

The minimal form needs `name` and `value`, and when given like this it is used
as a comparison (`==`), where only listings that match the value exactly are
considered. This works best for fields such as `objectType` where we're
interested in an exact match.

You can also use `max` and `min` to define maximum and minimum thresholds for
the field. For example if we're appraising a house of 100m² but we don't get
enough exact matches, we could set `max` to 110 and `min` to 90 to widen the
range a bit.

When `[max,min]_relative` is True the corresponding threshold is considered a
relative one. So for example if we're appraising a house of 200m² and we
don't get enough exact matches, we can set `max` and `min` to `10`, and both
`max_relative` and `min_relative` to True. This way the thresholds will be at
180m² and 220m² respectively. Relative thresholds can oly be used with
numerical fields, and cause an error if used on other types.

When the field is a datetime (stored in the db as a string) we can still use
`min` and `max` since the date format is sortable (`YYYY-MM-DD HH:MM:SS`).
    """
    name: str
    value: int | str
    max: int | str | None = None
    min: int | str | None = None
    max_relative: bool = False
    min_relative: bool = False


@app.get("/")
async def root():
    return {"message": "See /docs for more info"}


@app.post("/appraise")
async def appraise(area: str, fields: List[AppraiseField] = None):
    """Given an area, it either returns the appraisal for that area, or if
needed triggers a new fetch. If a new fetch is triggered (or one is ongoing), a
new request for appraisal should be submitted once the fetch is done (use `GET
/fetch_status` to check the status of a fetch.

    """
    response = requests.get(
        f"https://api.booli.se/areas?q={area}&{booli_api_auth()}")
    if response.status_code != 200:
        raise RuntimeError(
            f"Fetching the areas endpoint for {area} got {response.status_code}")
    try:
        area = response.json()["areas"][0]["name"]
    except IndexError:
        raise HTTPException(status_code=404, detail=f"Area {area} not found")

    status = await fetch_status(area)
    status = status["status"]
    if status == "DONE":
        # Run the appraisal algorithm
        listings = []
        sold = []
        async for entry in db.listings.find({"fetchMetadata.area": area}, {"_id": False}):
            if fields:
                skip = False
                for field in fields:
                    if entry.get(field.name) is None:
                        # don't consider entries missing the field
                        skip = True
                        break
                    if field.max is None and field.min is None:
                        # exact match
                        if entry.get(field.name) != field.value:
                            skip = True
                            break
                    else:
                        # ranged match
                        if field.max is not None:
                            if field.max_relative:
                                # relative threshold
                                if not isinstance(field.value, numbers.Number):
                                    raise HTTPException(
                                        status_code=418, detail=f"Ranged limit for value of type {type(field.value)} doesn't make sense!")
                                if entry.get(field.name) > field.value * (1 + (field.max / 100)):
                                    skip = True
                                    break
                            else:
                                # absolute threshold
                                if entry.get(field.name) > (field.value + field.max if isinstance(field.max, int) else field.max):
                                    skip = True
                                    break
                        if field.min is not None:
                            if field.min_relative:
                                # relative threshold
                                if not isinstance(field.value, numbers.Number):
                                    raise HTTPException(
                                        status_code=418, detail=f"Ranged limit for value of type {type(field.value)} doesn't make sense!")
                                if entry.get(field.name) < field.value * (1 - (field.min / 100)):
                                    skip = True
                                    break
                            else:
                                # absolut threshold
                                if entry.get(field.name) < (field.value - field.min if isinstance(field.min, int) else field.min):
                                    skip = True
                                    break
                if skip:
                    continue
            if entry["fetchMetadata"]["endpoint"] == "/listings":
                listings.append(entry)
            else:
                sold.append(entry)
        # TODO : This is an oversimplified algo, it returns the median price
        # for all the entries considered. There's probably a more meaningful
        # algorithm to use here.
        sold = [entry["soldPrice"] for entry in sold if entry.get("soldPrice")]
        sold_n = len(sold)
        if sold_n:
            sold = statistics.median(sold)
        else:
            sold = 0
        listings = [entry["listPrice"]
                    for entry in listings if entry.get("listPrice")]
        listings_n = len(listings)
        if listings_n:
            listings = statistics.median(listings)
        else:
            listings = 0
        return {"data": {"listings": {"median_price": listings, "entries_considered": listings_n}, "sold": {"median_price": sold, "entries_considered": sold_n}}}
    elif status == "ONGOING":
        return {"status": f"A fetch for {area} is ongoing. Try again later."}
    elif status == "MISSING":
        fetch.delay(area)
        return {"status": f"A fetch for {area} has been triggered."}
    # TODO : Implement logic for OUTDATED!


@app.get("/fetch_status")
async def fetch_status(area: str):
    """Given an area, it returns the status for the area's fetch.

    MISSING  -> this area has never been fetched
    ONGOING  -> this area is being fetched
    DONE     -> this area has been fetched
    # Not implemented yet:
    OUTDATED -> this area has been fetched, the data is outdated

All the states except `DONE` and `ONGOING` imply that a call to `GET /appraise`
for that area will trigger a new fetch.
    """
    response = requests.get(
        f"https://api.booli.se/areas?q={area}&{booli_api_auth()}")
    if response.status_code != 200:
        raise RuntimeError(
            f"Fetching the areas endpoint for {area} got {response.status_code}")
    try:
        area = response.json()["areas"][0]["name"]
    except IndexError:
        raise HTTPException(status_code=404, detail=f"Area {area} not found")

    result = await db.fetches.find_one({"area": response.json()["areas"][0]["name"]}, {"_id": False})
    if not result:
        return {"status": "MISSING"}
    else:
        if (result["sold_status"] == "started" or result["listings_status"] == "started"):
            return {"status": "ONGOING"}
        elif (result["sold_status"] == "done" and result["listings_status"] == "done"):
            return {"status": "DONE"}
