"""This module defines the Celery tasks used by the VärderingsAPI.
"""


import hashlib
import os
import secrets
import sys
import time
from datetime import datetime

import requests
from celery import Celery
from pymongo import MongoClient, UpdateOne, TEXT


def check_env():
    """This function checks that all needed envvars are set, otherwise
prints an error message and exits.
    """
    for envvar in (
            "BROKER_HOST",
            "BOOLI_CALLER_ID",
            "BOOLI_PRIVATE_KEY",
            "MONGO_HOST",
            "MONGO_DB",
            "CELERY_RATE_LIMIT"):
        if not os.getenv(envvar):
            print(f"Environment variable {envvar} isn't set!")
            sys.exit(1)


def booli_api_auth():
    """This function uses BOOLI_CALLER_ID and BOOLI_PRIVATE_KEY envvars, as
well as an ad-hoc generated unique secret token and timestamp, and returns a
string that can be injected into the URL, containing the query params for auth
against the Booli API.
    """
    caller_id = os.getenv("BOOLI_CALLER_ID")
    private_key = os.getenv("BOOLI_PRIVATE_KEY")
    now = str(int(time.time()))
    unique = secrets.token_urlsafe(16)

    hash_ = hashlib.new("sha1")
    hash_.update("".join((caller_id, now, private_key, unique)).encode())
    digest = hash_.hexdigest()

    return f"callerId={caller_id}&time={now}&unique={unique}&hash={digest}"


# This is what I would usually put inside a `if __name__ == "__main__"`, but
# due to celery it needs to be at the top level.
#

check_env()

db = MongoClient(os.getenv("MONGO_HOST"))[os.getenv("MONGO_DB")]
# Make sure the needed indexes are in place
db.fetches.create_index("area", unique=True)
db.listings.create_index("booliId", unique=True)
db.listings.create_index([("fetchMetadata.area", TEXT)])

app = Celery('tasks', broker_url=f"pyamqp://{os.getenv('BROKER_HOST')}")

#
# End of "__main__"-block


@app.task
def fetch(area):
    """Given an area (name) run a fetch for that area, caching both /listings and
/sold from the Booli API.
    """
    db.fetches.update_one({"area": area}, {
        "$set": {"timestamp": datetime.now(), "area": area, "sold_status": "started", "listings_status": "started"}},
        upsert=True)
    for endpoint in ("/listings", "/sold"):
        print(f"Starting fetch for {endpoint} in {area}...")
        request_page.delay(endpoint, area, 0)


@app.task(rate_limit=os.getenv("CELERY_RATE_LIMIT"))
def request_page(endpoint, area, offset):
    """Given an endpoint in the Booli API, an area (name) and an offset,
request the data and save it to the db.
    """
    limit = 500
    url = f"https://api.booli.se{endpoint}?q={area}&limit={limit}&offset={offset}&{booli_api_auth()}"
    print(f"Requesting offset {offset} for {endpoint} {area}")
    response = requests.get(url, headers={"User-Agent": "giacomo-testar"})

    if response.status_code != 200:
        raise RuntimeError(f"Got {response.status_code} for {url}")
    else:
        data = response.json()

        # Save the data to the db
        listings = data[endpoint[1:]]

        for listing in listings:
            listing["fetchMetadata"] = dict()
            listing["fetchMetadata"]["area"] = area
            listing["fetchMetadata"]["fetched"] = datetime.now()
            listing["fetchMetadata"]["endpoint"] = endpoint
            operations = [UpdateOne({"booliId": listing["booliId"]}, {
                "$set": listing}, upsert=True) for listing in listings]
            db.listings.bulk_write(operations)

        if offset == 0:
            while offset < data["totalCount"]:
                offset += limit
                # Trigger fetch for next page
                request_page.delay(endpoint, area, offset)
            # Trigger one extra time to set the fetch as done
            request_page.delay(endpoint, area, offset)
        elif offset > data["totalCount"]:
            db.fetches.update_one(
                {"area": area}, {"$set": {"timestamp": datetime.now(), f"{endpoint[1:]}_status": "done"}})
            print(f"Fetch done for {area} {endpoint}")
            return
