# Bostadsvärdering - coding exercise

## Exercise description - Bostadsvärdering

Ta fram en tjänst med ett API för värdering av bostäder. Data att använda som
underlag finns att hämta från Boolis API (se https://www.booli.se/p/api/ för
dokumentation). Givet uppgifter om en bostad (position, storlek, avgift etc)
ska tjänsten presentera en värdering baserad på historiska försäljningspriser.
Lagra datan som används för underlag (t.ex. i en databas) så att den inte
behöver laddas ned vid varje värdering. Det viktiga är inte att få en så
korrekt värdering som möjligt utan att visa hur problemet kan lösas. Det är
fritt fram att utforma API:et som du själv tycker är lämpligt. Redogör för de
antaganden och begränsningar du gör. Uppgiften innefattar inga krav på
utveckling av användargränssnitt.

## Design overview

Here are the main moving parts for this application:

- a **MongoDB** used to cache the data fetched from the Booli API
- a **RESTful API** that receives requests from the clients/frontends. The API uses
the data in the DB to appraise an object based on different parameters. It also
triggers a new fetch of data from the Boolio API when needed (each fetch job is
a Celery task)
- one or more **Celery workers** to execute our tasks
- a **RabbitMQ** instance used by Celery to pass tasks data around

The API will implement the following endpoints:
	- `POST /appraise?area=xyz` : Given an area, and optionally other
fields to filter by (give in the request JSON body) to refine the appraisal
algorithm, it returns either an appraisal of the object described OR a message
about a new fetch triggered.
	- `GET /fetch_status?area=xyz` : Used to check the status of a certain
area fetch.

Each timestamped fetch by area is saved to the db. One a client requests a new
appraise, first the db is checked for a previous fetch for the area. If the
fetch isn't present, a new fetch is triggered.

A fetch is a Celery task that scrapes all `/listing` and `/sold` from the Booli
API for a certain area. The fetch itself only saves some metadata (`area`, the
strings used to trigger the fetch, and a status such as `done` or `started` for
both `/listings` and `/sold`; as well as a timestamp.

Another collection in the DB holds all the objects scraped from both `/listing`
and `/sold`.

Each distinct part (API, DB, Celery worker, RabbitMQ) can scale independently.

You can test the latest version of this API here:
https://booli.grinton.dev/docs .


## Deploy the stack

Using docker-compose:

1. Run `docker compose pull` and `docker compose build` to fetch and build all
the needed images
2. `cp dotenv .env` and fill in all the needed variables
3. `docker compose up -d`

Points 1 and 3 can be run together with `docker compose up -d --build`.
