This directory contains the presentation for the project.

It's written in markdown and pandoc can be used to generate a pdf or a revealjs
presentation.

Example:

```bash
docker run --rm --volume "`pwd`:/data" --user `id -u`:`id -g` pandoc/latex README.md -o README.pdf
```

```bash
docker run --rm --volume "`pwd`:/data" pandoc/core -t revealjs -s -o presentation.html presentation.md -V revealjs-url=https://unpkg.com/reveal.js/ --slide-level=3
```