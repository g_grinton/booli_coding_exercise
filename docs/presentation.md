---
author: Giacomo Turatto
title: Bostadsvärdering - coding exercise
subtitle:
date: 2023-05-15
---

# Disclaimer

I spent more than 5 hours on this project.

Probably somewhere around ~3 hours on general design, reflecting on the
problem; and ~10 hours on implementation and testing.

## Disclaimer #2

To manage to make something remotely interesting in a weekend's worth of evening
time, I had to prioritize some things away.

## Stuff I prioritized away

![](https://imgs.xkcd.com/comics/code_lifespan.png)

- unit tests
- CI/CD

If I had to keep developing this, I'd start by fixing those first.

# The general design

## Moving parts

### REST API

What would be exposed to clients/frontends.

The API makes sure each `area` is fetched using the right name as to avoid
duplicate fetches.

### Celery tasks and workers

Take care of fetching data from the Booli API.

### MongoDB

To store data from the Booli API.

Might have used something else, but since it works with JSON natively, I chose
to keep it simple.

An alternative could have been PostgreSQL (which supports JSON data fields).

### RabbitMQ

Used by Celery.

### Caddy

As a reverse proxy to provide automagic TLS for our API.

# Reflections

## Possible improvements

- Provide the clients/frontends with a way of knowing the progress/ETA on a
fetch.

- The appraising algorithm could be improved.

- Possible duplicate fetches (i.e. Nacka och Stockholm)

# LIVE DEMO

![](https://media.tenor.com/HR3YZjYlr_UAAAAM/nervous-sweating-sweating-bullets.gif)

# Questions?



# That's all, folks!

![](https://i.imgur.com/h117iPk.gif)